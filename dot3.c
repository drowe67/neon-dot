#include <stdio.h>
#include <arm_neon.h>

#define OUTER   1000000
#define INNER   1000
#define VEC_LEN 4

float dot_func(float x[],  float y[], int n);

int main() {
   float __attribute__((aligned(16))) x[INNER*VEC_LEN];
   float __attribute__((aligned(16))) y[INNER*VEC_LEN];
   float sum = 0.0;
   int i;

   //printf("x: 0x%x y: 0x%x\n", (uint32_t)x, (uint32_t)y);

   for(i=0; i<INNER*VEC_LEN; i++) {
      x[i] = y[i] = 1.0;
   }
   for(i=0; i<OUTER; i++) {
      sum += dot_func(x,y,INNER);
   }

   printf("sum: %g target cycles: %g FLOPS: %g\n", sum, (float)OUTER*INNER, (float)2*VEC_LEN*OUTER*INNER);
}
