#include <stdio.h>
#include <arm_neon.h>

#define OUTER 1000
#define INNER 1000000

float dot(float32x4_t v1,  float32x4_t v2, int n) {
   float32x4_t v3, v4, v5, v6, v7;
   int i;
   
   for(i=0; i<4; i++) {
      v3[i] = v4[i] = v5[i] = v6[i] = v7[i] = 0;
   }
   n /= 10;
   for(i=0; i<n; i++) {
      v3 = vmlaq_f32(v3, v1, v2);
      v4 = vmlaq_f32(v4, v1, v2);
      v5 = vmlaq_f32(v5, v1, v2);
      v6 = vmlaq_f32(v6, v1, v2);
      v7 = vmlaq_f32(v7, v1, v2);
      v3 = vmlaq_f32(v3, v1, v2);
      v4 = vmlaq_f32(v4, v1, v2);
      v5 = vmlaq_f32(v5, v1, v2);
      v6 = vmlaq_f32(v6, v1, v2);
      v7 = vmlaq_f32(v7, v1, v2);
   }

   float sum = 0.0;
   for(i=0; i<4; i++)
      sum += v3[i]+v4[i]+v5[i]+v6[i]+v7[i];
   return sum;
}

int main() {
   float32x4_t v1 = { 1.0, 1.0, 1.0, 1.0 };
   float32x4_t v2 = { 1.0, 1.0, 1.0, 1.0 };
   float sum = 0.0;
   int i;
   
   for(i=0; i<OUTER; i++) {
      sum += dot(v1,v2,INNER);
   }

   printf("sum: %g FLOPS: %g\n", sum, (float)8*OUTER*INNER);
}
