/* Ne10 dot product assembler function */

#include <stdio.h>
#include "NE10.h"

#define NVECS   1000
#define VEC_LEN     4
#define ARR_LEN NVECS*VEC_LEN
#define OUTER   1000000

int main() {
    ne10_float32_t __attribute__((aligned(16))) src1[ARR_LEN];
    ne10_float32_t __attribute__((aligned(16))) src2[ARR_LEN];
    ne10_float32_t __attribute__((aligned(16))) dst[NVECS];
    int i;

    // printf("src1: 0x%0x src2: 0x%0x dst: 0x%0x\n", (uint32_t)src1, (uint32_t)src2, (uint32_t)dst);
    
    for(i=0; i<ARR_LEN; i++) {
       src1[i] = 1.0;
       src2[i] = 1.0;
    }
    /* this function doesn't return a completed dot product, but algorithm very close so a 
       reasonable comparison */
    for(i=0; i<OUTER; i++) 
       ne10_dot_vec4f_neon(dst,(ne10_vec4f_t*)src1,(ne10_vec4f_t*)src2,NVECS);
    
    printf("FLOPS: %g\n", (float)ARR_LEN*OUTER);
}
