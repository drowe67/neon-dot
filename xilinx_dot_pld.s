
@ from Xilinx App note XAPP1206 v1.1 June 12, 2014

	.align 4
	.global neon_dot_product_vec16_pld
	.arm
neon_dot_product_vec16_pld:
	pld [r0, #0]
	pld [r1, #0]
	pld [r0, #32]
	pld [r1, #32]
	vmov.i32	q10, #0
	vmov.i32	q11, #0
	vmov.i32	q12, #0
	vmov.i32	q13, #0

.L_mainloop_vec_16_pld:
	@ load current set of values
	vldm r0!, {d0, d1, d2, d3, d4, d5, d6, d7}
	vldm r1!, {d10, d11, d12, d13, d14, d15, d16, d17}
	pld [r0]
	pld [r1]
	pld [r0, #32]
	pld [r1, #32]

	@ calculate values for current set
	vmla.f32	q10, q0, q5
	vmla.f32	q11, q1, q6
	vmla.f32	q12, q2, q7
	vmla.f32	q13, q3, q8
	@ loop control
	subs		r2, r2, #16
	bgt		.L_mainloop_vec_16_pld @ loop if r2 > 0, if we have more elements to process
.L_return_vec_16_pld:
	@ calculate the final result
	vadd.f32	q15, q10, q11
	vadd.f32	q15, q15, q12	
	vadd.f32	q15, q15, q13	
	vpadd.f32	d30, d30, d31
	vpadd.f32	d30, d30, d30		
	vmov.32		r0, d30[0]
	@ return 
	bx		lr
