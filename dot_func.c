#include <arm_neon.h>

float dot_func(float x[],  float y[], int n) {
   float32x4_t a1, a2, a3, a4, x1, y1;
   int i;
   
   for(i=0; i<4; i++) {
      a1[i] = a2[i] = a3[i] = a4[i] = 0;
   }
   n >>=2;
   for(i=n; i>0; i--) {
      x1 = vld1q_f32(x); x+=4;
      y1 = vld1q_f32(y); y+=4;
      a1 = vmlaq_f32(a1, x1, y1);

      x1 = vld1q_f32(x); x+=4;
      y1 = vld1q_f32(y); y+=4;
      a2 = vmlaq_f32(a2, x1, y1);

      x1 = vld1q_f32(x); x+=4;
      y1 = vld1q_f32(y); y+=4;
      a3 = vmlaq_f32(a3, x1, y1);
      
      x1 = vld1q_f32(x); x+=4;
      y1 = vld1q_f32(y); y+=4;
      a4 = vmlaq_f32(a4, x1, y1);
   }

   float sum = 0.0;
   for(i=0; i<4; i++)
      sum += a1[i] + a2[i] + a3[i] + a4[i];
   return sum;
}
