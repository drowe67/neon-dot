#include <stdio.h>
#include <arm_neon.h>

#define OUTER 1000
#define INNER 1000000

float dot(float32x4_t v1, float32x4_t v2, int n) {
   float32x4_t v3 = { 0.0, 0.0, 0.0, 0.0 };
   int i;
   
   for(i=0; i<n; i++) {
      v3 = vmlaq_f32(v3, v1, v2);
    }

   float sum = 0.0;
   for(i=0; i<4; i++)
      sum += v3[i];
   return sum;
}

int main() {
   float32x4_t v1 = { 1.0, 1.0, 1.0, 1.0 };
   float32x4_t v2 = { 1.0, 1.0, 1.0, 1.0 };
   float sum = 0.0;
   int i;
   
   for(i=0; i<OUTER; i++) {
      sum += dot(v1,v2,INNER);
   }

   printf("sum: %g FLOPS: %g\n", sum, (float)8*OUTER*INNER);
}
