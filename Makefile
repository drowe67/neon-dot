# dot product experients on a RPi Model B
# David Rowe Dec 2018

CC=gcc
CFLAGS=-Wall -W -O3 -g -W -Wextra
CFLAGS+=-mfpu=neon -march=armv8-a -mtune=cortex-a53
CFLAGS+=-I../Ne10/inc/
NEON_LIB=../Ne10/build/modules/libNE10.a

EXE=dot1 dot2 dot3 dot4 dotne10 dotxilinx
all:  $(EXE)

# single instruction inner loop now memory reads
dot1: dot1.o
	$(CC) -o $@ $(CFLAGS) $<
dot2: dot2.o
	$(CC) -o $@ $(CFLAGS) $<

# reading two source vectors from memory
dot3: dot3.o dot_func.o
	$(CC) -o $@ $(CFLAGS) dot3.o dot_func.o
dot4: dot3.o dot_func.s
	$(CC) -o $@ $(CFLAGS) dot3.o dot_func.s
dotne10: dotne10.c
	$(CC) -o $@ $(CFLAGS) $< $(NEON_LIB)
dotxilinx: dotxilinx.c xilinx_dot_pld.s
	$(CC) -o $@ $(CFLAGS) $< xilinx_dot_pld.s

test: all
	@perf_4.9 stat -e cycles,instructions ./dot1 2> dot_log.txt
	@perf_4.9 stat -e cycles,instructions ./dot2 2>> dot_log.txt
	@perf_4.9 stat -e cycles,instructions ./dot3 2>> dot_log.txt
	@perf_4.9 stat -e cycles,instructions ./dot4 2>> dot_log.txt
	@perf_4.9 stat -e cycles,instructions ./dotne10 2>> dot_log.txt
	@perf_4.9 stat -e cycles,instructions ./dotxilinx 2>> dot_log.txt
	grep cycles dot_log.txt

clean:
	rm -f $(EXE) *.o


