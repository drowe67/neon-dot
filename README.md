# NEON Dot

Software to test optimisation of dot product code written in ARM NEON for the Raspberry Pi.
See [ARM Neon optimisation](http://www.rowetel.com/?p=6577).

# Usage

You will need:
1. perf
2. The Ne10 project built for the last test, however for the first four tests just gcc is required

$ make test
