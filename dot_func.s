	.arch armv8-a
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.file	"dot_func.c"
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.align	2
	.global	dot_func
	.syntax unified
	.arm
	.fpu neon
	.type	dot_func, %function
dot_func:
.LFB2009:
	.file 1 "dot_func.c"
	.loc 1 3 0
	.cfi_startproc
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL0:
	push	{r4, r5, r6, lr}
	.cfi_def_cfa_offset 16
	.cfi_offset 4, -16
	.cfi_offset 5, -12
	.cfi_offset 6, -8
	.cfi_offset 14, -4
	.loc 1 10 0
	asr	r2, r2, #2
	cmp	r2, #0
	vmov.f32	q8, #0.0  @ v4sf
	sub	sp, sp, #64
	vstr	d16, [sp, #48]
	vstr	d17, [sp, #56]
	vstr	d16, [sp, #32]
	vstr	d17, [sp, #40]
	vstr	d16, [sp, #16]
	vstr	d17, [sp, #24]
	vst1.64	{d16-d17}, [sp:64]
	ble	.L2
	vmov	q11, q8  @ v4sf
	vmov	q10, q8  @ v4sf
	vmov	q9, q8  @ v4sf

	@ main loop, with some manual optimisaton
.L3:
	vld1.32	{d24-d25}, [r0:128]!
	subs	r2, r2, #1
	vld1.32	{d26-d27}, [r1:128]!
	vld1.32	{d2-d3}, [r0:128]!
	vld1.32	{d0-d1}, [r1:128]!
	vld1.32	{d6-d7}, [r0:128]!
	vld1.32	{d4-d5}, [r1:128]!
	vld1.32	{d28-d29}, [r0:128]
	vld1.32	{d30-d31}, [r1:128]!
	vmla.f32	q11, q1, q0
	vmla.f32	q10, q3, q2
	vmla.f32	q8, q12, q13
	vmla.f32	q9, q14, q15
	bne	.L3

.L2:
	vstr	d22, [sp, #16]
	vstr	d23, [sp, #24]
	vstr	d20, [sp, #32]
	vstr	d21, [sp, #40]
	vstr	d18, [sp, #48]
	vstr	d19, [sp, #56]
	vst1.64	{d16-d17}, [sp:64]
	vldr.32	s15, [sp, #16]
	vldr.32	s14, [sp]
	vldr.32	s11, [sp, #32]
	vldr.32	s12, [sp, #4]
	vldr.32	s0, [sp, #20]
	vadd.f32	s14, s14, s15
	vldr.32	s13, [sp, #48]
	vldr.32	s10, [sp, #36]
	vldr.32	s15, [sp, #24]
	vadd.f32	s0, s0, s12
	vldr.32	s12, [sp, #8]
	vadd.f32	s14, s14, s11
	vldr.32	s8, [sp, #52]
	vldr.32	s11, .L7
	vldr.32	s9, [sp, #40]
	vadd.f32	s0, s0, s10
	vadd.f32	s15, s15, s12
	vadd.f32	s14, s14, s13
	vldr.32	s12, [sp, #28]
	vldr.32	s13, [sp, #12]
	vldr.32	s10, [sp, #56]
	vadd.f32	s0, s0, s8
	vadd.f32	s15, s15, s9
	vadd.f32	s14, s14, s11
	vldr.32	s11, [sp, #44]
	vadd.f32	s13, s13, s12
	vldr.32	s12, [sp, #60]
	vadd.f32	s15, s15, s10
	vadd.f32	s0, s0, s14
	vadd.f32	s14, s13, s11
	vadd.f32	s15, s15, s0
	vadd.f32	s0, s14, s12
	vadd.f32	s0, s0, s15
	add	sp, sp, #64
	@ sp needed
	pop	{r4, r5, r6, pc}
.L8:
	.align	2
.L7:
	.word	0
	.cfi_endproc
